
package require sqlite3
${log}::info [modinfo sqlite3]

namespace eval settings {
    variable database_file "[dict get $state_dict program_name].db"

    proc create_new_db {} {

	# Open a connection to the database file
	sqlite3 db $settings::database_file
	# Single quotes in SQLite are around string literals.  The name of
	# the column should be in single quotes.
	#
	# Name of the program
	lappend column_list "'program' TEXT"

	# Path to the executables
	lappend column_list "'path' TEXT"

	# Create the table
	db eval "CREATE TABLE 'paths' ([join $column_list ", "])"

	# Create entry for avrdude path
	db eval "INSERT INTO 'paths' VALUES('avrdude','')"

	# Create entry for bootloader path
	db eval "INSERT INTO 'paths' VALUES('bootloader','')"

	# Create entry for application path
	db eval "INSERT INTO 'paths' VALUES('application','')"

	# Miscellaneous settings
	db eval "CREATE TABLE 'settings' ('key' TEXT, 'value' TEXT)"

	db eval "INSERT INTO 'settings' VALUES('ISP programmer','')"

	############################ Baud rates #############################

	set table_name "Baud rates"
	set column_list [list]

	lappend column_list "'key' TEXT"
	lappend column_list "'value' TEXT"

	# Create the table
	db eval "CREATE TABLE '$table_name' ([join $column_list ", "])"

	# Command interface
	db eval "INSERT INTO '$table_name' VALUES('Command', '')"

	# Debug interface
	db eval "INSERT INTO '$table_name' VALUES('Debug', '')"

	db close
    }

    proc check_avrdude_path {} {
	global state_dict
	global log
	global avrdude_path_led_canvas
	set avrdude_path [dict get $state_dict avrdude_path]
	# puts "avrdude path is $avrdude_path"

	if {$::tcl_platform(os) eq "Windows NT"} {
	    # Check if the file exists
	    set file_exists [file exists $avrdude_path]
	    
	    # Check if the file ends in avrdude.exe
	    set file_ends_in_avrdude [string equal [file tail $avrdude_path] "avrdude.exe"]	    
	} else {
	    # Is the path empty?
	    if {$avrdude_path eq ""} {
		set file_exists false
	    } else {
		# See if the file is exectuable
		set avrdude_which_path [exec which $avrdude_path]
		set file_exists [file exists $avrdude_which_path]
		set file_ends_in_avrdude [string equal [file tail $avrdude_which_path] "avrdude"]	
	    }
	    
	}
	if {$file_exists && $file_ends_in_avrdude} {
	    # Things look fine
	    status_leds::set_led $avrdude_path_led_canvas true
	    ${log}::info "Found avrdude at $avrdude_path"
	} else {
	    status_leds::set_led $avrdude_path_led_canvas false
	    ${log}::info "Failed to find avrdude executable"
	}
    }

    proc check_app_hex_path {} {
	global state_dict
	global log
	global app_hex_path_led_canvas
	set app_hex_path [dict get $state_dict app_hex_path]
	# Check if the file exists
	set file_exists [file exists $app_hex_path]

	# Check if the file ends in *.hex
	set file_extension_is_hex [string equal [file extension $app_hex_path] ".hex"]

	if {$file_exists && $file_extension_is_hex} {
	    # Things look fine
	    status_leds::set_led $app_hex_path_led_canvas true
	    ${log}::info "Found application hex at $app_hex_path"
	} else {
	    status_leds::set_led $app_hex_path_led_canvas false
	    ${log}::info "Failed to find application hex"
	}
    }

}

if {[file exists $settings::database_file]} {
    # Open a connection to the database file
    sqlite3 db $settings::database_file

    # Read the avrdude path setting
    set program_path [db onecolumn {SELECT path FROM paths WHERE program='avrdude'}]
    dict set state_dict avrdude_path $program_path

    # Read the bootloader path setting
    set program_path [db onecolumn {SELECT path FROM paths WHERE program='bootloader'}]
    dict set state_dict boot_hex_path $program_path

    # Read the application path setting
    set program_path [db onecolumn {SELECT path FROM paths WHERE program='application'}]
    dict set state_dict app_hex_path $program_path

    # Read the ISP programmer setting
    try {
	set isp_programmer [db onecolumn {SELECT value FROM settings WHERE key='ISP programmer'}]
	dict set state_dict isp_programmer $isp_programmer
    } trap {} {} {
	db eval "INSERT INTO 'settings' VALUES('ISP programmer','')"
	dict set state_dict isp_programmer ""
    }

    db close

} else {
    # Make a new database
    settings::create_new_db
}
