
namespace eval leds {

    variable led_diameter 20

    proc create_led {canvas_widget color} {
	# Return a led object in the supplied canvas widget.  The LED
	# will fill the canvas.
	#
	# Arguments:
	#   canvas_widget -- The widget created to hold the LED
	#   color -- Color of the LED
	set offset 2
	set canvas_width [$canvas_widget cget -width]
	set x1 $offset
	set y1 $offset
	set x2 $canvas_width
	set y2 $canvas_width
	$canvas_widget create oval $x1 $y1 $x2 $y2 -fill $color -tags "led"
	return $canvas_widget
    }

    proc set_led_color {canvas_widget color} {
	# Change the color of the LED inside the canvas widget
	$canvas_widget itemconfigure led -fill $color
    }
}
