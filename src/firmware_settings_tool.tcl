toplevel .firmware_settings_tool
menu .firmware_settings_tool.menubar
.firmware_settings_tool configure -menu .firmware_settings_tool.menubar -height 150

wm title .firmware_settings_tool "Firmware settings"

# Help menu
menu .firmware_settings_tool.menubar.help -tearoff 0
.firmware_settings_tool.menubar add cascade -label Help -menu .firmware_settings_tool.menubar.help -underline 0
.firmware_settings_tool.menubar.help add command -label "About the firmware settings tool..." \
    -underline 0 -command firmware_settings_tool::help

namespace eval firmware_settings_tool {
    variable entry_width 5

    variable serial_number 0

    variable firmware_version "??"

    variable app_hex_path [file tail [dict get $state_dict app_hex_path]]



    proc test_settings {} {
	global log
	global state_dict	
	if {[dict get $state_dict command_channel] eq ""} {
	    # We haven't set up a channel
	    usb::open_command_channel
	} else {
	    ${log}::debug "CDC-ACM channel already open"
	}	  
	set firmware_settings_tool::firmware_version [usb::get_firmware_version]
	set firmware_settings_tool::serial_number [usb::get_serial_number]
    }

    
    proc write_serial_number {} {
	global log
	global state_dict
	if {[dict get $state_dict command_channel] eq ""} {
	    # We didn't find an ACM node.  Try to find it.
	    usb::open_command_channel
	    if {[dict get $state_dict command_channel] eq ""} {
		${log}::error "No command CDC-ACM channel found"
		return	
	    }
	}
	set channel [dict get $state_dict command_channel]
	usb::set_serial_number $firmware_settings_tool::serial_number
    }
    
    proc cancel {} {
	# Kill the window without writing the settings
	destroy .firmware_settings_tool
    }
}

########################### Create widgets ###########################

# Firmware version
ttk::label .firmware_settings_tool.firmware_version_label \
    -text "Firmware version"

ttk::label .firmware_settings_tool.firmware_version_entry \
    -textvariable firmware_settings_tool::firmware_version \
    -width $firmware_settings_tool::entry_width \
    -justify right \
    -font big_entry_font \
    -state readonly

# Serial number
ttk::label .firmware_settings_tool.serial_number_label \
    -text "Serial number"

ttk::entry .firmware_settings_tool.serial_number_entry \
    -textvariable firmware_settings_tool::serial_number \
    -width $firmware_settings_tool::entry_width \
    -justify right \
    -font big_entry_font \
    -state focus

ttk::button .firmware_settings_tool.write_serial_number_button \
    -text "Write" \
    -command firmware_settings_tool::write_serial_number

# Test and Cancel buttons
ttk::button .firmware_settings_tool.firmware_settings_tool_test_button \
    -text "Test" \
    -command firmware_settings_tool::test_settings

ttk::button .firmware_settings_tool.cancel_button \
    -text "Cancel" \
    -command firmware_settings_tool::cancel

########################## Position widgets ##########################

set firmware_settings_tool_row 0

grid columnconfigure .firmware_settings_tool 1 -weight 1

# Firmware version
grid config .firmware_settings_tool.firmware_version_label \
    -column 0 -row $firmware_settings_tool_row \
    -sticky "e" \
    -padx 5 -pady 5

grid config .firmware_settings_tool.firmware_version_entry \
    -column 1 -row $firmware_settings_tool_row \
    -sticky "ew" \
    -padx 5 -pady 5

# Serial number setting
incr firmware_settings_tool_row
grid config .firmware_settings_tool.serial_number_label \
    -column 0 -row $firmware_settings_tool_row \
    -sticky "e" \
    -padx 5 -pady 5

grid config .firmware_settings_tool.serial_number_entry \
    -column 1 -row $firmware_settings_tool_row \
    -sticky "ew" \
    -padx 5 -pady 5

grid config .firmware_settings_tool.write_serial_number_button \
    -column 2 -row $firmware_settings_tool_row \
    -sticky "e" \
    -padx 5 -pady 5


# Test button
incr firmware_settings_tool_row
grid config .firmware_settings_tool.firmware_settings_tool_test_button \
    -column 1 -row $firmware_settings_tool_row \
    -sticky "e" \
    -padx 5 -pady 5

grid config .firmware_settings_tool.cancel_button \
    -column 2 -row $firmware_settings_tool_row \
    -sticky "e" \
    -padx 5 -pady 5

# Get rid of the namespace when the window is closed
bind .firmware_settings_tool <Destroy> {
    # tcl calls this script multiple times.  Only delete the namespace
    # if it still exists.
    if {[namespace exists firmware_settings_tool]} {
	namespace delete firmware_settings_tool
    }
}

raise .firmware_settings_tool
after 100 firmware_settings_tool::test_settings
