toplevel .isp_programmer_editor
menu .isp_programmer_editor.menubar
.isp_programmer_editor configure -menu .isp_programmer_editor.menubar -height 150

wm title .isp_programmer_editor "Configure ISP programmer"

# Help menu
menu .isp_programmer_editor.menubar.help -tearoff 0
.isp_programmer_editor.menubar add cascade -label Help -menu .isp_programmer_editor.menubar.help -underline 0
.isp_programmer_editor.menubar.help add command -label "About the ISP programmer editor..." \
    -underline 0 -command isp_programmer_editor::help

namespace eval isp_programmer_editor {

    # Width of the path entry box
    variable entry_width 80

    variable isp_programmer [dict get $state_dict isp_programmer]


    proc help {} {
	# What to execute when Help-->About is selected
	#
	# Arguments:
	#   None
	global log
	tk_messageBox -message "ISP programmer editor" \
	    -title "ISP programmer editor"
    }

    proc set_avrdude_path {} {
	# This will just set the value in the entry field.  Entries
	# won't be active until the OK button is pressed.
	global log
	global state_dict
	set avrdude_path [tk_getOpenFile -defaultextension ".exe"]
	# Erase the old value
	.isp_programmer_editor.avrdude_path_entry delete 0 end
	# Write the new value
	.isp_programmer_editor.avrdude_path_entry insert 0 $avrdude_path
	raise .isp_programmer_editor
    }

    proc set_ftprog_path {} {
	# This will just set the value in the entry field.  Entries
	# won't be active until the OK button is pressed.
	global log
	global state_dict
	set ftprog_path [tk_getOpenFile -defaultextension ".exe"]
	# Erase the old value
	.isp_programmer_editor.ftprog_path_entry delete 0 end
	# Write the new value
	.isp_programmer_editor.ftprog_path_entry insert 0 $ftprog_path
	raise .isp_programmer_editor
    }

    proc set_boot_hex_path {} {
	# This will just set the value in the entry field.  Entries
	# won't be active until the OK button is pressed.
	global log
	global state_dict
	set boot_hex_path [tk_getOpenFile -defaultextension ".exe"]
	# Erase the old value
	.isp_programmer_editor.boot_hex_path_entry delete 0 end
	# Write the new value
	.isp_programmer_editor.boot_hex_path_entry insert 0 $boot_hex_path
	raise .isp_programmer_editor
    }

    proc set_app_hex_path {} {
	# This will just set the value in the entry field.  Entries
	# won't be active until the OK button is pressed.
	global log
	global state_dict
	set app_hex_path [tk_getOpenFile -defaultextension ".hex"]
	# Erase the old value
	.isp_programmer_editor.app_hex_path_entry delete 0 end
	# Write the new value
	.isp_programmer_editor.app_hex_path_entry insert 0 $app_hex_path
	raise .isp_programmer_editor
    }    

    

    proc write_db_values {} {
	# This will write the values to the database and to the global
	# state dictionary.  It will also kill the window.
	global log
	global state_dict
	# Open a connection to the database file
	sqlite3 db $settings::database_file

	# The ISP programmer
	db eval "UPDATE settings SET value='$isp_programmer_editor::isp_programmer' WHERE key='ISP programmer'"
	dict set state_dict isp_programmer $isp_programmer_editor::isp_programmer


	# Close the window
	destroy .isp_programmer_editor
	
    }

    proc cancel {} {
	# Kill the window without writing the settings
	destroy .isp_programmer_editor
    }

}

########################### Create widgets ###########################

# ISP programmer
ttk::label .isp_programmer_editor.isp_programmer_label \
    -text "ISP programmer"

ttk::combobox .isp_programmer_editor.isp_programmer_combobox \
    -values [list "Atmel ICE" "Pololu v2.1"] \
    -state readonly

.isp_programmer_editor.isp_programmer_combobox set [dict get $state_dict isp_programmer]

bind .isp_programmer_editor.isp_programmer_combobox <<ComboboxSelected>> {
    set isp_programmer_editor::isp_programmer [%W get]
    ${log}::debug "Set $isp_programmer_editor::isp_programmer"
}



# OK / cancel buttons
ttk::button .isp_programmer_editor.ok_button \
    -text "OK" \
    -command isp_programmer_editor::write_db_values

ttk::button .isp_programmer_editor.cancel_button \
    -text "Cancel" \
    -command isp_programmer_editor::cancel

########################## Position widgets ##########################

# Allow the path entry to resize
grid columnconfigure .isp_programmer_editor 1 -weight 1

# ISP programmer
set isp_programmer_editor_row 0
grid config .isp_programmer_editor.isp_programmer_label \
    -column 0 -row $isp_programmer_editor_row \
    -padx 5 -pady 5

grid config .isp_programmer_editor.isp_programmer_combobox \
    -column 1 -row $isp_programmer_editor_row \
    -padx 5 -pady 5




# OK / cancel buttons
incr isp_programmer_editor_row
grid config .isp_programmer_editor.ok_button \
    -column 1 -row $isp_programmer_editor_row \
    -sticky "e" \
    -padx 5 -pady 5

grid config .isp_programmer_editor.cancel_button \
    -column 2 -row $isp_programmer_editor_row \
    -padx 5 -pady 5
