
# Root window console is .console_frame.text

namespace eval text_console {
    proc cornerchar {} {
	# Return the character at the lower-left corner of the text entry
	# box.
	global log
	set cornerchar [.console_frame.text get "current lineend -1 chars"]
	return $cornerchar
    }

    proc manager {text} {
	# Send console text whereever it needs to go
	global log
	global state_dict

	# Make the timestamp for the log file
	set time_now_ms [clock milliseconds]
	set time_now_s [expr double($time_now_ms)/1000]
	# set ms_remainder [expr int($time_delta_s * 1000 - int($time_delta_s) * 1000)]
	set ms_remainder [expr $time_now_ms - int($time_now_ms / 1000) * 1000]
	set file_time_string [format "%s.%03d" [clock format [expr int($time_now_s)] -format \
					       "%Y-%m-%d %H:%M:%S"] $ms_remainder]
	set filemsg "\[ $file_time_string \] $text"

	set console_time_string "\[[clock format [clock seconds] -format %T]\]"

	foreach line [split $text "\n"] {
	    .console_frame.text insert end "$console_time_string $line\n"
	}
	# Scroll to the end
	.console_frame.text yview moveto 1


    }
}
