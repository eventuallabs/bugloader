namespace eval usb {

    proc detect_vcps {} {
	# Windows -- returns a dictionary of:
	#
	# (COM port): vendor_id, product_id, composite_index
	global log
	set vcp_dict [dict create]
	switch $::tcl_platform(os) {
	    {Linux} {
		set nodelist ""
		try {
		    # FTDI devices show up as ttyUSB*
		    set nodelist [glob -nocomplain -directory /dev/ ttyUSB*]
		    # Arduino devices show up as ttyACM*
		    lappend nodelist [glob -nocomplain -directory /dev/ ttyACM*]
		} trap {} {message optdict} {
		    ${log}::debug $message
		    set nodelist ""
		}
		foreach vcp [join $nodelist] {
		    try {
			set udev_data [exec udevadm info --query=property --name $vcp]
			set id_serial ""
			set id_usb_interface_number ""
			foreach line [split $udev_data "\n"] {
			    if {[string first "ID_SERIAL=" $line] >= 0} {
				set id_serial [lindex [split $line "= \n"] 1]
			    }
			    if {[string first "ID_USB_INTERFACE_NUM=" $line] >= 0} {
				set composite_index [string trimleft [lindex [split $line "= \n"] 1] 0]
			    }
			    if {[string first "ID_VENDOR_ID=" $line] >= 0} {
				set vendor_id [string trimleft [lindex [split $line "= \n"] 1] 0]
			    }
			    if {[string first "ID_MODEL_ID=" $line] >= 0} {
				set product_id [string trimleft [lindex [split $line "= \n"] 1] 0]
			    }

			}
		    } trap CHILDSTATUS {results options} {
			${log}::debug "oops -- $results"
			set status [lindex [dict get $options -errorcode] 2]
			continue
		    }
		    ${log}::debug "ID_SERIAL is $id_serial"
		    ${log}::debug "  Vendor ID: $vendor_id"
		    ${log}::debug "  Product ID: $product_id"
		    ${log}::debug "  COM port: $vcp"
		    ${log}::debug "  Composite index: $composite_index"
		    dict set vcp_dict $vcp vendor_id $vendor_id
		    dict set vcp_dict $vcp product_id $product_id
		    dict set vcp_dict $vcp composite_index $composite_index
		}
	    }
	    {Windows NT} {
		# Grab the device info set for present objects only
		set dfs [twapi::devinfoset -presentonly true]
		foreach element [twapi::devinfoset_elements $dfs] {
		    catch {twapi::devinfoset_element_registry_property $dfs $element class} classname
		    if {$classname eq "Ports"} {
			# USB/serial ports are part of the "ports" class
			catch {twapi::devinfoset_element_registry_property $dfs $element friendlyname} friendlyname
			${log}::debug "Friendlyname is $friendlyname"
			# Get the COMn number from the friendlyname.  Friendlynames look like
			#
			# USB Serial Device (COM5)
			set vcp [lindex [split $friendlyname "()"] 1]
			catch {twapi::devinfoset_element_registry_property $dfs $element mfg} mfg
			${log}::debug "  Manufacturer is $mfg"
			catch {twapi::devinfoset_element_registry_property $dfs $element hardwareid} hardwareid_list
			# There may be more than one entry in the hardare ID list.  Just use the first one.
			set hwid [lindex $hardwareid_list 0]
			${log}::debug "  Hardwareid is $hwid"
			set vendor_id [get_windows_hwid_value $hwid "VID_"]
			set product_id [get_windows_hwid_value $hwid "PID_"]
			set composite_index [get_windows_hwid_value $hwid "MI_"]
			${log}::debug "  Vendor ID: $vendor_id"
			${log}::debug "  Product ID: $product_id"
			${log}::debug "  COM port: $vcp"
			${log}::debug "  Composite index: $composite_index"
			dict set vcp_dict $vcp vendor_id $vendor_id
			dict set vcp_dict $vcp product_id $product_id
			dict set vcp_dict $vcp composite_index $composite_index
		    }
		}
		twapi::devinfoset_close $dfs
	    }
	    default {
		# This should never happen
		${log}::debug "Failed to detect OS"
	    }
	}

	return $vcp_dict
    }

    proc test_usb_connection {} {
	global log
	global state_dict
	global acm_node_status_led_canvas
	global bootloader_task_led_canvas
	${log}::info "Looking for CDC-ACM connection"
	set detection_list [detect_ports]
	if {[llength $detection_list] == 0} {
	    ${log}::error "No CDC-ACM ports found"
	    return
	}
	set found_count 0
	foreach node $detection_list {
	    set prefix [lindex [split [lindex $node 1] "-"] 0]
	    if {[string match $prefix ftdi]} {
		incr found_count
		set acm_node [lindex $node 0]
		lappend port_list $acm_node
		${log}::debug "Found FTDI device at $acm_node"
	    }
	}
	if {$found_count == 1} {
	    status_leds::set_led $acm_node_status_led_canvas true
	    dict set state_dict acm_connected true
	    ${log}::info "Setting CDC-ACM node to $acm_node"
	    dict set state_dict acm_node $acm_node
	} else {
	    status_leds::set_led $acm_node_status_led_canvas false
	    dict set state_dict acm_connected false
	}
	test_application
	if {[dict get $state_dict application_flashed]} {
	    # If the application has been flashed, the bootloader is there.
	    status_leds::set_led $bootloader_task_led_canvas true
	    dict set state_dict bootloader_flashed true
	}
    }

    proc get_command_vcp {} {
	# Return the Virtual Com Port for the command interface
	#
	global log
	global state_dict
	global acm_node_status_led_canvas
	# Arduinos with AVR-based USB/serial parts have multiple vendor IDs
	set vid_match_list [list 2A03 2341]
	# FTDI-based Arduinos have the FTDI vendor
	lappend vid_match_list 403
	set vcp_dict [detect_vcps]
	foreach {vcp} [dict keys $vcp_dict] {
	    ${log}::debug "Found $vcp"
	    set vid [dict get $vcp_dict $vcp vendor_id]
	    set pid [dict get $vcp_dict $vcp product_id]
	    set index [dict get $vcp_dict $vcp composite_index]
	    ${log}::debug "  Vendor ID: 0x$vid"
	    ${log}::debug "  Product ID: 0x$pid"
	    ${log}::debug "  Composite index: $index"
	    if {[lsearch -exact -nocase $vid_match_list $vid] >= 0} {
		${log}::info "Found Arduino at $vcp"
		status_leds::set_led $acm_node_status_led_canvas true
		${log}::info "Setting command CDC-ACM node to $vcp"
		dict set state_dict command_vcp $vcp
		return $vcp
	    }
	}
	# We made it through the loop.  That means we didn't find anything.
	status_leds::set_led $acm_node_status_led_canvas false
	dict set state_dict command_vcp ""
	return ""

    }

    proc get_pololu_uart_vcp {} {
	# Return the Virtual Com Port corresponding to the Pololu USB/UART interface
	#
	# The Pololu vendor ID is 1FFB
	global log
	global state_dict
	set vid_match_list [list 1FFB]
	set vcp_dict [detect_vcps]
	foreach {vcp} [dict keys $vcp_dict] {
	    ${log}::debug "Found $vcp"
	    set vid [dict get $vcp_dict $vcp vendor_id]
	    set pid [dict get $vcp_dict $vcp product_id]
	    set index [dict get $vcp_dict $vcp composite_index]
	    ${log}::debug "  Vendor ID: 0x$vid"
	    ${log}::debug "  Product ID: 0x$pid"
	    ${log}::debug "  Composite index: $index"
	    if {[lsearch -exact -nocase $vid_match_list $vid] >= 0} {
		if {$index == 3} {
		    # index = 1 is the ISP programmer, 3 is the UART
		    ${log}::info "Found Pololu v2.1 UART interface at $vcp"
		    dict set state_dict debug_vcp $vcp
		    return $vcp
		}
	    }
	}
	dict set state_dict debug_vcp ""
	return ""
    }

    proc test_bootloader {} {
	global log
	global state_dict
	global bootloader_task_led_canvas
	set avrdude [dict get $state_dict avrdude_path]
	set vcp [dict get $state_dict acm_node]
	set avrdude_exec_string "$avrdude "
	append avrdude_exec_string "-p m2560 -P $vcp -c arduino -b 230400"
	set fid [open "| $avrdude_exec_string 2>@1" r]
	while {[gets $fid line] != -1} {
	    # gets returns -1 if it encounters the end of output from
	    # the program.  If it returns > -1, then we've read a line
	    # of output and the characters are stored in the variable
	    # "line".

	    # Process the line read as desired...
	    append result $line
	    text_console::manager $line
	    update idletasks
	    if {[string first "programmer is not responding" $line] >= 0} {
		${log}::error "Can not connect to the bootloader"
		update idletasks
	    }
	}
	# Close our side of the pipe when we're done
	close $fid

	set success_string "Device signature = 0x1e9801 (probably m2560)"
	set bootloader_ok false
	foreach line [split $result "\n"] {
	    if {[string first $success_string $line] >= 0} {
		# We found the bootloader
		status_leds::set_led $bootloader_task_led_canvas true
		${log}::info "Bootloader has been flashed"
		dict set state_dict bootloader_flashed true
	    }
	}
	return
    }

    proc flash_application {} {
	global log
	global state_dict
	set application [dict get $state_dict app_hex_path]
	${log}::info "Flashing [file tail $application]"
	update idletasks
	set vcp [dict get $state_dict acm_node]
	set avrdude_exec_string "[dict get $state_dict avrdude_path] "
	append avrdude_exec_string "-p m2560 -P $vcp -c arduino -b 230400 -F -u "
	append avrdude_exec_string "-U flash:w:$application:a"

	set fid [open "| $avrdude_exec_string 2>@1" r]
	while {[gets $fid line] != -1} {
	    # gets returns -1 if it encounters the end of output from
	    # the program.  If it returns > -1, then we've read a line
	    # of output and the characters are stored in the variable
	    # "line".

	    # Process the line read as desired...
	    text_console::manager $line
	    update idletasks
	}
	# Close our side of the pipe when we're done
	close $fid

    }

    proc format_acm_node {alias} {
	# Returns a string able to be used by open to open a hardware
	# connection.
	#
	# Arguments:
	#
	# alias -- Short connection name returned by get_potential_aliases
	global log
	global state_dict
	switch $::tcl_platform(os) {
	    {Linux} {
		set nodename $alias
	    }
	    {Windows NT} {
		set nodename \\\\.\\[string toupper $alias]
	    }
	}

	return $nodename
    }

    proc get_windows_hwid_value {hwid delimiter} {
	# Return a value from a windows hardware ID
	#
	# Arguments:
	#   hwid -- Windows hardware ID, like USB\VID_1FFB&PID_00BB&REV_0102&MI_01
	#   delimiter -- Substring like "VID_" or "PID_"

	# Prefix string will start with the value we want terminated in a &
	set prefix_string [lindex [textutil::splitx $hwid $delimiter] 1]
	set value [string trimleft [lindex [split $prefix_string &] 0] 0]
	return $value
    }

    proc close_command_acm_channel {} {
	global log
	global state_dict
	set channel [dict get $state_dict command_channel]
	if {$channel ne ""} {
	    ${log}::debug "Trying to close channel $channel"
	} else {
	    ${log}::debug "No explicit channel.  Other channels are [chan names]"
	}

	try {
	    ${log}::debug "Closing CDC-ACM channel"
	    chan close $channel
	} trap {} {message optdict} {
	    ${log}::debug "No channel to close"
	    dict set state_dict command_channel ""
	}
	dict set state_dict command_channel ""
    }

    proc close_debug_acm_channel {} {
	global log
	global state_dict
	set channel [dict get $state_dict debug_channel]
	if {$channel ne ""} {
	    ${log}::debug "Trying to close channel $channel"
	} else {
	    ${log}::debug "No explicit channel.  Other channels are [chan names]"
	}

	try {
	    ${log}::debug "Closing CDC-ACM channel"
	    chan close $channel
	} trap {} {message optdict} {
	    ${log}::debug "No channel to close"
	    dict set state_dict debug_channel ""
	}
	dict set state_dict debug_channel ""
    }

    proc wait_for_data {channel chars timeout_ms} {
	# Busy loop until a byte is ready to be read or until timeout
	#
	# Arguments:
	#  channel -- Channel created with open and configured with chan configure
	#  chars -- Number of characters to read
	#  timeout_ms -- Timeout in milliseconds
	#
	# Don't use the event loop for this, since Tcl might decide to
	# slip another channel transaction in during the wait --
	# screwing up return values.
	global log
	set start_time_ms [clock milliseconds]
	set channel_data ""
	while {! [string length $channel_data]} {
	    try {
		set channel_data [chan read $channel $chars]
	    } trap {} {message optdict} {
		${log}::error "Error reading from channel: $message"
	    }
	    set elapsed_time_ms [expr [clock milliseconds] - $start_time_ms]
	    if {$elapsed_time_ms >= $timeout_ms} {
		${log}::error "Channel did not respond within $timeout_ms ms"
		return
	    }
	}
	return $channel_data
    }

    proc read_until_timeout {channel timeout_ms} {
	# Read all data from the channel until there's a gap lasting timeout_ms
	#
	# Arguments:
	#  channel -- Channel created with open and configured with chan configure
	#  timeout_ms -- Timeout in milliseconds
	global log
	set start_time_ms [clock milliseconds]
	set times_up false
	set channel_data ""
	while {! $times_up} {
	    set new_data [chan read $channel]
	    set elapsed_time_ms [expr [clock milliseconds] - $start_time_ms]
	    if { [string length $new_data] } {
		# We got new data, reset the timer
		${log}::debug "New data at $elapsed_time_ms ms"
		update idletasks
		set start_time_ms [clock milliseconds]
		append channel_data $new_data
	    }

	    if {$elapsed_time_ms >= $timeout_ms} {
		set times_up true
	    }
	}
	return $channel_data
    }

    proc open_command_channel {} {
	global log
	global state_dict
	global application_task_led_canvas
	global acm_node_status_led_canvas
	if {[dict get $state_dict command_vcp] eq ""} {
	    # There's no command CDC-ACM node.  Try looking for one
	    get_command_vcp
	    if {[dict get $state_dict command_vcp] eq ""} {
		# We tried and failed to find a command CDC-ACM node
		${log}::error "No command VCP found"
		return
	    }
	}
	set node [format_acm_node [dict get $state_dict command_vcp]]
	try {
	    set channel [open $node r+]
	} trap {} {message optdict} {
	    # Trap the usage signal, print the message, and exit the application.
	    # Note: Other errors are not caught and passed through to higher levels!
	    ${log}::error $message
	    # We couldn't open the channel, which means there's something wrong with the node
	    status_leds::set_led $acm_node_status_led_canvas false
	    dict set state_dict command_vcp ""
	    dict set $state_dict command_channel ""
	    return
	}
	set baud [dict get $state_dict command_interface_baud]
	${log}::debug "Trying to configure $node"
	update idletasks
	chan configure $channel -mode "${baud},n,8,1" \
	    -buffering none \
	    -blocking 0 \
	    -translation auto
	${log}::debug "Channel configured using [dict get $state_dict command_vcp]"
	update idletasks
	# Wait for unsolicited data to come through, or timeout if there's no data
	set data [wait_for_data $channel 5 5000]
	# Clear out the Rx queue
	append data [read_until_timeout $channel 100]
	${log}::debug "Unsolicited data after boot: [string trim $data]"
	puts -nonewline $channel "\r*idn?\r"
	set data [string trim [read_until_timeout $channel 200]]
	text_console::manager $data
	${log}::debug "Response to *idn? was $data"
	set success_string "Eventual Labs"
	if {[string first $success_string $data] >= 0} {
	    # This is a Eventual Labs device and a valid channel
	    status_leds::set_led $application_task_led_canvas true
	    ${log}::info "Application has been flashed"
	    dict set state_dict application_flashed true
	    dict set state_dict command_channel $channel
	    return $channel
	} else {
	    ${log}::error "No Eventual Labs device found at [dict get $state_dict command_vcp]"
	    dict set $state_dict command_channel ""
	    chan close $channel
	    return ""
	}
    }

    proc open_debug_channel {} {
	global log
	global state_dict
	global application_task_led_canvas
	if {[dict get $state_dict debug_vcp] eq ""} {
	    # There's no debug CDC-ACM node.  Try looking for one
	    get_pololu_uart_vcp
	    if {[dict get $state_dict debug_vcp] eq ""} {
		# We tried and failed to find a debug CDC-ACM node
		${log}::error "No debug CDC-ACM port found"
		return
	    }
	}
	set node [format_acm_node [dict get $state_dict debug_vcp]]
	try {
	    set channel [open $node r+]
	} trap {} {message optdict} {
	    # Trap the usage signal, print the message, and exit the application.
	    # Note: Other errors are not caught and passed through to higher levels!
	    ${log}::debug $message
	    return
	}
	set baud [dict get $state_dict debug_interface_baud]
	${log}::debug "Trying to configure $node"
	update idletasks
	try {
	    chan configure $channel -mode "${baud},n,8,1" \
		-buffering none \
		-blocking 0 \
		-translation auto
	} trap {} {message optdict} {
	    ${log}::error $message
	    dict set state_dict debug_channel ""
	    return ""
	}
	${log}::debug "Channel configured using [dict get $state_dict debug_vcp]"
	update idletasks
	# We have no way of further verifying the debug port.
	dict set state_dict debug_channel $channel
	return $channel

    }

    proc test_application {} {
	global log
	global state_dict
	# The application is flashed if we can open a CDC-ACM channel
	# and it responds appropriately.
	open_acm_channel
    }

    proc get_serial_number {} {
	global log
	global state_dict
	set channel [dict get $state_dict command_channel]
	${log}::info "Querying serial number..."
	update idletasks
	puts -nonewline $channel "*idn?\r"
	after 500
	set data [chan gets $channel]
	text_console::manager $data
	${log}::debug "Response to *idn? was $data"
	set sernum_string [lindex [split $data ","] 2]
	set sernum [lindex [split $sernum_string "SN"] end]
	${log}::debug "Serial number is $sernum"
	return $sernum
    }

    proc get_firmware_version {} {
	global log
	global state_dict
	set channel [dict get $state_dict command_channel]
	${log}::info "Querying firmware version..."
	update idletasks
	puts -nonewline $channel "*idn?\r"
	after 500
	set data [chan gets $channel]
	text_console::manager $data
	${log}::debug "Response to *idn? was $data"
	set version_string [lindex [split $data ","] 3]
	${log}::debug "Version number is $version_string"
	return $version_string
    }

    proc set_serial_number {serial_number} {
	# Set the serial number via the SERNUM command
	#
	# Arguments: serial_number -- Integer serial number
	#
	# These remote-command commands assume all the connection
	# dependancies are handled at the tool level -- like in
	# firmware_settings_tool.
	global log
	global state_dict
	set channel [dict get $state_dict command_channel]
	${log}::debug "Setting serial number..."
	update idletasks
	puts -nonewline $channel "sernum $serial_number\r"
	after 500
	set data [chan gets $channel]
	text_console::manager $data
	if {$data ne "0"} {
	    ${log}::error "Return is $data -- serial number not set"
	    return error
	} else {
	    ${log}::debug "Set serial number to $serial_number"
	    update idletasks
	    return ok
	}
    }

}
