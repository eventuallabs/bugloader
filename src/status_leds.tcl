
namespace eval status_leds {

    variable on_color "light green"
    variable off_color "red"

    proc set_led {led_canvas boolean} {
	if $boolean {
	    leds::set_led_color $led_canvas $status_leds::on_color
	} else {
	    leds::set_led_color $led_canvas $status_leds::off_color
	}
    }
}
