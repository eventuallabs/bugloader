toplevel .terminal_tool
menu .terminal_tool.menubar
.terminal_tool configure -menu .terminal_tool.menubar -height 150

wm title .terminal_tool "Terminal tool"

# Help menu
menu .terminal_tool.menubar.help -tearoff 0
.terminal_tool.menubar add cascade -label Help -menu .terminal_tool.menubar.help -underline 0
.terminal_tool.menubar.help add command -label "About the terminal tool..." \
    -underline 0 -command terminal_tool::help

namespace eval terminal_tool {
    variable entry_width 80

    # Command history
    variable history_list [list]

    variable history_index 0

    # Clear terminal icon
    variable clear_terminal_icon_file icons/view-refresh.png
    variable clear_terminal_icon [image create photo -format png -file $clear_terminal_icon_file]

    proc cancel {} {
	# Kill the window without writing the settings
	destroy .terminal_tool
    }

    proc open_command_channel {} {
	usb::open_command_channel
	toggle_command_open_button
    }

    proc open_debug_channel {} {
	global state_dict
	global log
	usb::open_debug_channel
	toggle_debug_open_button
	try {
	    set channel [dict get $state_dict debug_channel]
	    # Get text line by line as it becomes available
	    chan event $channel readable {terminal_tool::read_debug_line}
	} trap {} {message optdict} {
	    ${log}::error $message
	}

    }

    proc close_command_acm_channel {} {
	usb::close_command_acm_channel
	toggle_command_open_button
    }

    proc close_debug_acm_channel {} {
	usb::close_debug_acm_channel
	toggle_debug_open_button
    }

    proc toggle_command_open_button {} {
	global log
	global state_dict
	if {[dict get $state_dict command_channel] eq ""} {
	    .terminal_tool.channel_frame.command_frame.open_button configure -text "Open"
	    .terminal_tool.channel_frame.command_frame.open_button \
		configure -command terminal_tool::open_command_channel
	} else {
	    .terminal_tool.channel_frame.command_frame.open_button \
		configure -text "Close"
	    .terminal_tool.channel_frame.command_frame.open_button \
		configure -command terminal_tool::close_command_acm_channel
	}
    }

    proc toggle_debug_open_button {} {
	global log
	global state_dict
	if {[dict get $state_dict debug_channel] eq ""} {
	    .terminal_tool.channel_frame.debug_frame.open_button configure -text "Open"
	    .terminal_tool.channel_frame.debug_frame.open_button configure -command terminal_tool::open_debug_channel
	} else {
	    .terminal_tool.channel_frame.debug_frame.open_button configure -text "Close"
	    .terminal_tool.channel_frame.debug_frame.open_button configure -command terminal_tool::close_debug_acm_channel
	}
    }

    proc send_command {text} {
	# Commands get sent to the command interface
	global log
	global state_dict
	if {[dict get $state_dict command_channel] eq ""} {
	    # We didn't find an ACM node.  Try to find it.
	    usb::open_command_channel
	    if {[dict get $state_dict command_channel] eq ""} {
		${log}::error "No command CDC-ACM channel found"
		return
	    } else {
		toggle_command_open_button
	    }
	}
	set channel [dict get $state_dict command_channel]
	# Clear out the receive buffer
	puts -nonewline $channel "\r"
	chan read $channel
	puts -nonewline $channel "$text\r"
	lappend terminal_tool::history_list $text
	set terminal_tool::history_index 0

	# Get text line by line as it becomes available
	chan event $channel readable {terminal_tool::read_command_line}
    }

    proc command_text_manager {text} {
	# Remove carriage returns and newlines before writing to the text widget
	set textline [string map {"\r" "" "\n" ""} $text]
	if {$textline eq ""} {
	    # This text output was just a newline or a carriage
	    # return.  Don't write anything.
	    return
	}
	.terminal_tool.command_terminal_frame.text insert end "$textline\n"
	# Scroll to the end
	.terminal_tool.command_terminal_frame.text yview moveto 1
    }

    proc debug_text_manager {text} {
	# Remove carriage returns and newlines before writing to the text widget
	set textline [string map {"\r" "" "\n" ""} $text]
	if {$textline eq ""} {
	    # This text output was just a newline or a carriage
	    # return.  Don't write anything.
	    return
	}
	# Make the timestamp
	set time_now_ms [clock milliseconds]
	set time_now_s [expr double($time_now_ms)/1000]
	# set ms_remainder [expr int($time_delta_s * 1000 - int($time_delta_s) * 1000)]
	set ms_remainder [expr $time_now_ms - int($time_now_ms / 1000) * 1000]
	set time_string [format "%s.%03d" [clock format [expr int($time_now_s)] -format \
					   "%Y-%m-%d %H:%M:%S"] $ms_remainder]
	set textline "\[ $time_string \] $textline"
	.terminal_tool.debug_terminal_frame.text insert end "$textline\n"
	.terminal_tool.debug_terminal_frame.text tag add timetag \
	    {insert linestart -1 lines +2 chars} \
	    {insert linestart -1 lines +26 chars}
	.terminal_tool.debug_terminal_frame.text tag configure timetag -foreground blue
	# Scroll to the end
	.terminal_tool.debug_terminal_frame.text yview moveto 1
    }

    proc read_command_line {} {
	# Read a line from the command interface
	global log
	global state_dict
	set channel [dict get $state_dict command_channel]
	set data [chan gets $channel]
	${log}::debug "Received $data"
	command_text_manager $data
    }



    proc read_debug_line {} {
	# Read a line from the debug interface
	global log
	global state_dict
	set channel [dict get $state_dict debug_channel]
	set data [chan gets $channel]
	# Data from the USB/serial device is often just blank lines.
	# I think this is just noise from USB.  Reject it.
	
	# Why are strings coming out with ^Z characters?
	set data [lindex [split $data "\u001A"] 0]

	# Remove carriage returns and newlines.
	set textline [string map [list "\r" "" "\n" ""] $data]

	if {$textline eq ""} {
	    # This debug output was just a newline or a carriage
	    # return.  That means this was noise.  Don't write anything.
	    return
	}
	${log}::debug "Received $data"
	debug_text_manager $data
    }

    proc insert_previous_history_command {} {
	# Erase the last line
	.terminal_tool.command_terminal_frame.text delete {insert linestart} end
	# Move to the next line
	.terminal_tool.command_terminal_frame.text insert end "\n"
	set command [lindex $terminal_tool::history_list end-$terminal_tool::history_index]
	.terminal_tool.command_terminal_frame.text insert end "$command"
	if {[lindex $terminal_tool::history_list end-[expr $terminal_tool::history_index + 1]] ne ""} {
	    incr terminal_tool::history_index
	}
    }

    proc insert_next_history_command {} {
	# Erase the last line
	.terminal_tool.command_terminal_frame.text delete {insert linestart} end
	# Move to the next line
	.terminal_tool.command_terminal_frame.text insert end "\n"
	set command [lindex $terminal_tool::history_list end-$terminal_tool::history_index]
	.terminal_tool.command_terminal_frame.text insert end "$command"
	if {[lindex $terminal_tool::history_list end-[expr $terminal_tool::history_index - 1]] ne ""} {
	    incr terminal_tool::history_index -1
	}
    }

    proc clear_command_text {} {
	# Erase the command text box
	.terminal_tool.command_terminal_frame.text delete 0.0 end
    }
    
    proc clear_debug_text {} {
	# Erase the debug text box
	.terminal_tool.debug_terminal_frame.text delete 0.0 end
    }
}

########################### Create widgets ###########################

# The channel frame
ttk::label .terminal_tool.channel_frame_label \
    -text "Channel" \
    -font frame_font

ttk::labelframe .terminal_tool.channel_frame \
    -labelwidget .terminal_tool.channel_frame_label \
    -labelanchor n \
    -borderwidth 1 \
    -relief sunken

set command_channel_frame_label [ttk::label .terminal_tool.channel_frame.command_frame_label \
				     -text "Command interface" \
				     -font frame_font]

ttk::labelframe .terminal_tool.channel_frame.command_frame \
    -labelwidget $command_channel_frame_label \
    -labelanchor n \
    -borderwidth 1 \
    -relief sunken

# The debug interface channel frame
ttk::label .terminal_tool.channel_frame.debug_frame_label \
    -text "Debug interface" \
    -font frame_font

ttk::labelframe .terminal_tool.channel_frame.debug_frame \
    -labelwidget .terminal_tool.channel_frame.debug_frame_label \
    -labelanchor n \
    -borderwidth 1 \
    -relief sunken

# Open / close command interface
ttk::button .terminal_tool.channel_frame.command_frame.open_button \
    -text "Open" \
    -command usb::open_command_channel

# Open / close debug interface
ttk::button .terminal_tool.channel_frame.debug_frame.open_button \
    -text "Open" \
    -command usb::open_debug_channel

# The command terminal frame
ttk::label .terminal_tool.command_terminal_frame_label \
    -text "Command Terminal" \
    -font frame_font

ttk::labelframe .terminal_tool.command_terminal_frame \
    -labelwidget .terminal_tool.command_terminal_frame_label \
    -labelanchor n \
    -borderwidth 1 \
    -relief sunken

text .terminal_tool.command_terminal_frame.text -yscrollcommand {.terminal_tool.command_terminal_frame.scrollbar set} \
    -height 20 \
    -width 80 \
    -font terminal_font

scrollbar .terminal_tool.command_terminal_frame.scrollbar \
    -orient vertical \
    -command {.terminal_tool.command_terminal_frame.text yview}

# The debug terminal frame
ttk::label .terminal_tool.debug_terminal_frame_label \
    -text "Debug Terminal" \
    -font frame_font

ttk::labelframe .terminal_tool.debug_terminal_frame \
    -labelwidget .terminal_tool.debug_terminal_frame_label \
    -labelanchor n \
    -borderwidth 1 \
    -relief sunken

text .terminal_tool.debug_terminal_frame.text -yscrollcommand {.terminal_tool.debug_terminal_frame.scrollbar set} \
    -height 10 \
    -width 80 \
    -font log_font

scrollbar .terminal_tool.debug_terminal_frame.scrollbar \
    -orient vertical \
    -command {.terminal_tool.debug_terminal_frame.text yview}

# Create buttons to clear debug and command terminals
set clear_command_text_button [ttk::button .terminal_tool.command_terminal_frame.clear_button \
				 -image $terminal_tool::clear_terminal_icon \
				 -command terminal_tool::clear_command_text]

set clear_debug_text_button [ttk::button .terminal_tool.debug_terminal_frame.clear_button \
				 -image $terminal_tool::clear_terminal_icon \
				 -command terminal_tool::clear_debug_text]

tooltip::tooltip $clear_command_text_button "Clear command text"
tooltip::tooltip $clear_debug_text_button "Clear debug text"

ttk::button .terminal_tool.cancel_button \
    -text "Cancel" \
    -command terminal_tool::cancel

########################## Position widgets ##########################

set terminal_tool_row 0

grid columnconfigure .terminal_tool 0 -weight 1

grid config .terminal_tool.channel_frame \
    -column 0 \
    -row $terminal_tool_row \
    -columnspan 1 -rowspan 1 \
    -padx $widget_padding -pady $widget_padding \
    -sticky "snew"

# Give weight to the command frame column to allow it to center itself
grid columnconfigure .terminal_tool.channel_frame 0 -weight 1

grid config .terminal_tool.channel_frame.command_frame \
    -column 0 \
    -row 0 \
    -columnspan 1 -rowspan 1 \
    -padx $widget_padding -pady $widget_padding

# Give weight to the button column to allow it to center itself.
grid columnconfigure .terminal_tool.channel_frame.command_frame 0 -weight 1

grid config .terminal_tool.channel_frame.command_frame.open_button \
    -column 0 -row 0 \
    -padx 5 -pady 5

# Give weight to the debug frame column to allow it to center itself
grid columnconfigure .terminal_tool.channel_frame 1 -weight 1

grid config .terminal_tool.channel_frame.debug_frame \
    -column 1 \
    -row 0 \
    -columnspan 1 -rowspan 1 \
    -padx $widget_padding -pady $widget_padding

# Give weight to the button column to allow it to center itself.
grid columnconfigure .terminal_tool.channel_frame.debug_frame 0 -weight 1

grid config .terminal_tool.channel_frame.debug_frame.open_button \
    -column 0 -row 0 \
    -padx 5 -pady 5

# Command terminal
incr terminal_tool_row
grid config .terminal_tool.command_terminal_frame \
    -column 0 \
    -row $terminal_tool_row \
    -columnspan 1 -rowspan 1 \
    -padx $widget_padding -pady $widget_padding \
    -sticky "snew"

grid config .terminal_tool.command_terminal_frame.text \
    -column 0 \
    -row 0 \
    -columnspan 1 -rowspan 1 \
    -padx $widget_padding -pady $widget_padding \
    -sticky "snew"

grid config .terminal_tool.command_terminal_frame.scrollbar \
    -column 0 \
    -row 0 \
    -columnspan 1 -rowspan 1 \
    -padx $widget_padding -pady $widget_padding \
    -sticky "nse"

grid config .terminal_tool.command_terminal_frame.clear_button \
    -column 0 \
    -row 1 \
    -columnspan 1 -rowspan 1 \
    -padx $widget_padding -pady $widget_padding

# Debug terminal
incr terminal_tool_row
grid config .terminal_tool.debug_terminal_frame \
    -column 0 \
    -row $terminal_tool_row \
    -columnspan 1 -rowspan 1 \
    -padx $widget_padding -pady $widget_padding \
    -sticky "snew"

grid config .terminal_tool.debug_terminal_frame.text \
    -column 0 \
    -row 0 \
    -columnspan 1 -rowspan 1 \
    -padx $widget_padding -pady $widget_padding \
    -sticky "snew"

grid config .terminal_tool.debug_terminal_frame.scrollbar \
    -column 0 \
    -row 0 \
    -columnspan 1 -rowspan 1 \
    -padx $widget_padding -pady $widget_padding \
    -sticky "nse"

grid config .terminal_tool.debug_terminal_frame.clear_button \
    -column 0 \
    -row 1 \
    -columnspan 1 -rowspan 1 \
    -padx $widget_padding -pady $widget_padding

# Allow the text widgets to expand
grid rowconfigure .terminal_tool $terminal_tool_row -weight 1
grid rowconfigure .terminal_tool.command_terminal_frame 0 -weight 1
grid columnconfigure .terminal_tool.command_terminal_frame 0 -weight 1

grid rowconfigure .terminal_tool.debug_terminal_frame 0 -weight 1
grid columnconfigure .terminal_tool.debug_terminal_frame 0 -weight 1

incr terminal_tool_row
grid config .terminal_tool.cancel_button \
    -column 0 -row $terminal_tool_row \
    -sticky "e" \
    -padx 5 -pady 5

# Get rid of the namespace when the window is closed
bind .terminal_tool <Destroy> {
    global state_dict
    global log
    set channel [dict get $state_dict command_channel]
    try {
	# Turn off the event handler
	chan event $channel readable ""
    } trap {} {message optdict} {
	${log}::debug "No channel to generate events"
    }
    set channel [dict get $state_dict debug_channel]
    try {
	# Turn off the event handler
	chan event $channel readable ""
    } trap {} {message optdict} {
	${log}::debug "No channel to generate events"
    }
    # tcl calls this script multiple times.  Only delete the namespace
    # if it still exists.
    if {[namespace exists terminal_tool]} {
	namespace delete terminal_tool
    }

}

# Bind carriage returns to sending the last line of text over the
# serial port.
bind .terminal_tool.command_terminal_frame.text <Return> \
    {terminal_tool::send_command [.terminal_tool.command_terminal_frame.text \
				      get {insert linestart} {insert lineend}]}

# Bind up arrow to the last command
bind .terminal_tool.command_terminal_frame.text <Up> {
    terminal_tool::insert_previous_history_command
    # Prevent the default keybinding from acting
    break
}

bind .terminal_tool.command_terminal_frame.text <Down> {
    terminal_tool::insert_next_history_command
    # Prevent the default keybinding from acting
    break
}

raise .terminal_tool
focus .terminal_tool.command_terminal_frame.text
terminal_tool::toggle_command_open_button
terminal_tool::toggle_debug_open_button
