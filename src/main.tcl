
# --------------------- Global configuration --------------------------

# The name of this program.  This will get used to identify logfiles,
# configuration files and other file outputs.
set program_name bugloader

# This software's version.  Anything set here will be clobbered by the
# makefile when starpacks are built.
set revcode 1.0

# Set the log level.  Known values are:
# debug
# info
# notice
# warn
# error
# critical
# alert
# emergency
set loglevel debug

if {$tcl_platform(os) eq "Windows NT"} {
    # To access device information from the Windows registry
    package require twapi
}

# Provides splitx needed to split strings on substrings
package require textutil

#----------------------- Done with global configuration ---------------
package require Tk

# Create a dictionary to keep track of global state
# State variables:
#   program_name --  Name of this program (for naming the window)
#   program_version -- Version of this program
#   thisos  -- Name of the os this program is running on
#   serial_port_alias -- The human-readable name of the serial port connection
#   portchan -- The tcl channel name the tester is connected to
#   exelog -- The execution log filename
#   serlog -- The serial output log filename
#   device_name -- The device identifier to which the port is connected
#   device_version -- The firmware version of the connected device
#   device_baud -- Baud rate for device communication
#   device_character_delay_ms -- Delay between sent characters
set state_dict [dict create \
		    program_name $program_name \
		    program_version $revcode \
		    thisos $tcl_platform(os) \
		    serial_port_alias none \
		    debug_log_file none \
		    serlog none
	       ]

# Path to avrdude executable
dict set state_dict avrdude_path ""

# Path to application hex
dict set state_dict app_hex_path ""

# Has application been flashed?
dict set state_dict application_flashed false

# Is the programmer connected to a USB cable?  This doesn't indicate
# whether or not the programmer is connected to the PCB
dict set state_dict programmer_connected_via_usb false

# Is the programmer connected to the uC via ISP?
dict set state_dict programmer_connected_via_isp false

# Is there a CDC-ACM connection?
dict set state_dict acm_connected false

# CDC-ACM connection node to the command interface.  This may or not
# be configured into an active channel.
dict set state_dict command_vcp ""

# CDC-ACM connection node to the debug interface.  This may or not
# be configured into an active channel.
dict set state_dict debug_vcp ""

# Command CDC-ACM channel.  This can either be a Tcl channel or ""
dict set state_dict command_channel ""

# Debug CDC-ACM channel.  This can either be a Tcl channel or ""
dict set state_dict debug_channel ""

# Are the fuses set?
dict set state_dict fuses_set false

# ISP programmer
dict set state_dict isp_programmer "Pololu v2.1"

# ISP programmer vitual com port
dict set state_dict isp_programmer_vcp ""

# Command interface baud rate
dict set state_dict command_interface_baud "38400"

# Debug interface baud rate
dict set state_dict debug_interface_baud "19200"

# High fuse value (hex)
dict set state_dict high_fuse_hex "0xd8"

# Low fuse value (hex)
dict set state_dict low_fuse_hex "0xff"

# Extended fuse value (hex)
dict set state_dict extended_fuse_hex "0xfd"

dict set state_dict avrdude_mcu_name "m328p"

# General padding around widgets
set widget_padding 5

proc modinfo {modname} {
    # Return loaded module details.
    set modver [package require $modname]
    set modlist [package ifneeded $modname $modver]
    set modpath [lindex $modlist end]
    return "Loaded $modname module version $modver from ${modpath}."
}

proc source_tool {file_name} {
    global log
    global state_dict
    # Only source a tool script if it doesn't already exist
    #
    # Tool files must create namespaces named after their file names
    if {![namespace exists [file rootname $file_name]]} {
	# The window doesn't exist yet
	uplevel "source $file_name"
    } else {
	raise .[file rootname $file_name]
	${log}::error "[file rootname $file_name] already exists"
    }
}

############################### Fonts ################################

# This has to come before the logger setup, since the logger needs
# them
source fonts.tcl

# ------------------------ Set up validation --------------------------
source validate.tcl

# -------------------- Set up version comparisons ---------------------
source semver.tcl

################################ LEDs ################################

source leds.tcl
source status_leds.tcl

############################## tooltip ###############################
package require tooltip

############################# TTK themes #############################

# package require awthemes
# package require ttk::theme::awdark
# ttk::style theme use default

################################ Help ################################

source help.tcl

# Set up main window
source root_window.tcl
tkwait visibility .console_frame.text

source text_console.tcl

#----------------------------- Set up logger --------------------------

# The logging system will use the console text widget for visual
# logging.

package require logger
source lologger.tcl
${log}::info [modinfo logger]

# Testing the logger

.debug_frame.text insert end "Current loglevel is: [${log}::currentloglevel] \n"
${log}::info "Trying to log to [dict get $state_dict debug_log_file]"
${log}::info "Known log levels: [logger::levels]"
${log}::info "Known services: [logger::services]"
${log}::debug "Debug message"
${log}::info "Info message"
${log}::warn "Warn message"

# Can't tell which version of Tk we're using until the logger is set
# up.
${log}::info [modinfo Tk]
${log}::info "This is $program_name version $revcode running on [dict get $state_dict thisos]"
${log}::debug "Using TTK theme [ttk::style theme use]"

${log}::info [modinfo tooltip]

###################### Bring in saved settings #######################

source settings.tcl

################ Support for the hardware programmer #################

source programmer.tcl

source usb.tcl

# Startup tests
after 500 {
    settings::check_avrdude_path
    update idletasks
    settings::check_app_hex_path
    update idletasks
    update idletasks
}

vwait forever

