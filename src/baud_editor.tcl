toplevel .baud_editor
menu .baud_editor.menubar
.baud_editor configure -menu .baud_editor.menubar -height 150

wm title .baud_editor "Configure baud rates"

# Help menu
menu .baud_editor.menubar.help -tearoff 0
.baud_editor.menubar add cascade -label Help -menu .baud_editor.menubar.help -underline 0
.baud_editor.menubar.help add command -label "About baud settings..." \
    -underline 0 -command baud_editor::help

namespace eval baud_editor {

    # Width of the path entry box
    variable entry_width 6

    # Command interface baud
    variable command_baud [dict get $state_dict command_interface_baud]

    # Debug interface baud
    variable debug_baud [dict get $state_dict debug_interface_baud]

    proc help {} {
	# What to execute when Help-->About is selected
	#
	# Arguments:
	#   None
	global log
	tk_messageBox -message "Baud rate settings" \
	    -title "Baud rates"
    }

    proc write_db_values {} {
	# This will write the values to the database and to the global
	# state dictionary.  It will also kill the window.
	global log
	global state_dict
	# Open a connection to the database file
	sqlite3 db $settings::database_file

	# Command interface baud
	db eval "UPDATE 'Baud rates' SET value='$baud_editor::command_baud' WHERE key='Command'"
	dict set state_dict command_interface_baud $baud_editor::command_baud

	# Debug interface baud
	db eval "UPDATE 'Baud rates' SET value='$baud_editor::debug_baud' WHERE key='Debug'"
	dict set state_dict debug_interface_baud $baud_editor::debug_baud

	# Close the window
	destroy .baud_editor

    }

    proc cancel {} {
	# Kill the window without writing the settings
	destroy .baud_editor
    }

}

########################### Create widgets ###########################

# settings_list will have alternating label and entry widgets
set settings_list [list]

# Command interface baud label
lappend settings_list [ttk::label .baud_editor.command_label \
			  -text "Command interface"]

# Command interface baud entry
lappend settings_list [ttk::entry .baud_editor.command_entry \
			   -textvariable baud_editor::command_baud \
			   -width $baud_editor::entry_width \
			   -justify right \
			   -font big_entry_font \
			   -state focus]

# Debug interface baud label
lappend settings_list [ttk::label .baud_editor.debug_label \
			  -text "Debug interface"]

# Debug interface baud entry
lappend settings_list [ttk::entry .baud_editor.debug_entry \
			   -textvariable baud_editor::debug_baud \
			   -width $baud_editor::entry_width \
			   -justify right \
			   -font big_entry_font \
			   -state focus]


# OK / cancel buttons
ttk::button .baud_editor.ok_button \
    -text "OK" \
    -command baud_editor::write_db_values

ttk::button .baud_editor.cancel_button \
    -text "Cancel" \
    -command baud_editor::cancel

########################## Position widgets ##########################

# Allow the path entry to resize
grid columnconfigure .baud_editor 1 -weight 1

foreach {label entry} $settings_list {
    incr window_row
    grid config $label \
	-column 0 \
	-row $window_row \
	-padx $widget_padding -pady $widget_padding

    grid config $entry \
	-column 1 \
	-row $window_row \
	-padx $widget_padding -pady $widget_padding
}

# OK / cancel buttons
incr window_row
grid config .baud_editor.ok_button \
    -column 1 -row $window_row \
    -sticky "e" \
    -padx 5 -pady 5

grid config .baud_editor.cancel_button \
    -column 2 -row $window_row \
    -padx 5 -pady 5
