
namespace eval help {
    proc about_script {} {
	global log
	global state_dict
	global revcode
	tk_messageBox -message "[dict get $state_dict program_name]\nVersion $revcode" \
	-title "About [dict get $state_dict program_name]"
    }
}
