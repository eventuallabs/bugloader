namespace eval semver {

    
    proc major {semver} {
	# Return the major part of the semantic version
	return [lindex [split $semver .] 0]
    }

    proc minor {semver} {
	# Return the major part of the semantic version
	return [lindex [split $semver .] 1]
    }
    
    proc patch {semver} {
	# Return the major part of the semantic version
	return [lindex [split $semver .] 2]
    }

    proc greater {test_version min_version} {
	# Return the test version if the version is greater than or
	# equal to the minimum version.  0 if not.
	#
	# Arguments:
	#   test_version -- Semantic version number to be tested
	#   min_version -- Minimum compatible version
	if {[major $test_version] > [major $min_version]} {
	    # The test version is greater, so we're done
	    return $test_version
	} elseif {[major $test_version] < [major $min_version]} {
	    # The test version is less, so we're done
	    return 0
	} elseif {[minor $test_version] > [minor $min_version]} {
	    return $test_version
	} elseif {[minor $test_version] < [minor $min_version]} {
	    return 0
	} elseif {[patch $test_version] > [patch $min_version]} {
	    return $test_version
	} elseif {[patch $test_version] < [patch $min_version]} {
	    return 0
	} else {
	    # The versions are the same
	    return $test_version
	}
    }

}
