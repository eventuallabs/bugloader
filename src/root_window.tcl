
# Set window name
wm title . "[dict get $state_dict program_name]"

# menubar
menu .menubar
. configure -menu .menubar -height 150

# Settings menu item
menu .menubar.settings -tearoff 0
.menubar add cascade -label "Settings" -menu .menubar.settings
.menubar.settings add command -label "Paths" \
    -underline 0 -command "source path_editor.tcl"

# Choose the ISP programmer
.menubar.settings add command -label "ISP programmer" \
    -underline 0 -command "source isp_programmer_editor.tcl"

# Configure baud rates
.menubar.settings add command -label "Baud rates" \
    -underline 0 -command "source baud_editor.tcl"

# Tools menu item
menu .menubar.tools -tearoff 0
.menubar add cascade -label "Tools" -menu .menubar.tools

# ISP programmer menu item
.menubar.tools add command -label "ISP Programmer" \
    -underline 0 -command "source_tool programmer_tool.tcl"

# Firmware settings menu item
.menubar.tools add command -label "Firmware settings" \
    -underline 0 -command "source_tool firmware_settings_tool.tcl"

# Terminal tool menu item
.menubar.tools add command -label "Terminal" \
    -underline 0 -command "source_tool terminal_tool.tcl"

# Add help menu item
menu .menubar.help -tearoff 0
.menubar add cascade -label "Help" -menu .menubar.help
.menubar.help add command -label "About [dict get $state_dict program_name]..." \
    -underline 0 -command help::about_script

# The status frame
ttk::labelframe .status_frame -text "Status" \
    -labelanchor n \
    -borderwidth 1 \
    -relief groove

# The path settings frame
ttk::labelframe .status_frame.path_settings_frame -text "Path settings" \
    -labelanchor n \
    -borderwidth 1 \
    -relief groove

# The hardware status frame
ttk::labelframe .status_frame.hardware_status_frame -text "Hardware status" \
    -labelanchor n \
    -borderwidth 1 \
    -relief groove

# The provisioning tasks frame
ttk::labelframe .status_frame.provisioning_tasks_frame -text "Provisioning tasks" \
    -labelanchor n \
    -borderwidth 1 \
    -relief groove

# Task label
ttk::label .status_frame.tasks_label -text "Tasks" -font header_font

# avrdude status led
set avrdude_path_led_canvas \
    [canvas .status_frame.path_settings_frame.avrdude_status_led_canvas \
	 -width $leds::led_diameter -height $leds::led_diameter]
lappend path_settings_items [leds::create_led $avrdude_path_led_canvas red]

# avrdude status label
lappend path_settings_items [ttk::label .status_frame.path_settings_frame.avrdude_status_label \
				 -text "AVRDUDE" -font NameFont]

# Application hex path led
set app_hex_path_led_canvas \
    [canvas .status_frame.path_settings_frame.app_hex_path_led_canvas \
	 -width $leds::led_diameter -height $leds::led_diameter]
lappend path_settings_items [leds::create_led $app_hex_path_led_canvas red]

# Application hex path label
lappend path_settings_items [ttk::label .status_frame.path_settings_frame.app_hex_path_label \
				 -text "Application Hex" -font NameFont]

# ISP Programmer status led
set isp_programmer_status_led_canvas \
    [canvas .status_frame.hardware_status_frame.isp_programmer_status_led_canvas \
	 -width $leds::led_diameter -height $leds::led_diameter]
lappend hardware_status_items [leds::create_led $isp_programmer_status_led_canvas red]

# ISP Programmer status label
lappend hardware_status_items [ttk::label .status_frame.hardware_status_frame.isp_programmer_status_label \
				   -text "ISP Programmer" -font NameFont]

# ISP connection status LED
set isp_connection_status_led_canvas \
    [canvas .status_frame.hardware_status_frame.isp_status_led_canvas \
	 -width $leds::led_diameter -height $leds::led_diameter]
lappend hardware_status_items [leds::create_led $isp_connection_status_led_canvas red]

# ISP connection status label
lappend hardware_status_items [ttk::label .status_frame.hardware_status_frame.isp_connection_status_label \
				   -text "ISP connection" -font NameFont]

# CDC-ACM node status LED
set acm_node_status_led_canvas \
    [canvas .status_frame.hardware_status_frame.acm_node_status_led_canvas \
	 -width $leds::led_diameter -height $leds::led_diameter]
lappend hardware_status_items [leds::create_led $acm_node_status_led_canvas red]

# CDC-ACM node status label
lappend hardware_status_items [ttk::label .status_frame.hardware_status_frame.acm_node_status_label \
				   -text "CDC-ACM node" -font NameFont]

# Fuse programming task LED
set fuse_task_led_canvas \
    [canvas .status_frame.provisioning_tasks_frame.fuse_task_led_canvas \
	 -width $leds::led_diameter -height $leds::led_diameter]
lappend provisioning_task_items [leds::create_led $fuse_task_led_canvas red]

# Fuse programming status label
lappend provisioning_task_items [ttk::label .status_frame.provisioning_tasks_frame.fuse_status_label \
				     -text "Fuses programmed" -font NameFont]


# Application flashed status LED
set application_task_led_canvas \
    [canvas .status_frame.provisioning_tasks_frame.application_flashed_led_canvas \
	 -width $leds::led_diameter -height $leds::led_diameter]
lappend provisioning_task_items [leds::create_led $application_task_led_canvas red]

# Application flashed status label
lappend provisioning_task_items \
    [ttk::label .status_frame.provisioning_tasks_frame.application_flashed_label \
	 -text "Application flashed" -font NameFont]

# The console frame
ttk::labelframe .console_frame -text "Console" \
    -labelanchor n \
    -borderwidth 1 \
    -relief groove

text .console_frame.text -yscrollcommand {.console_frame.scrollbar set} \
    -height 20 \
    -width 150 \
    -font log_font

scrollbar .console_frame.scrollbar \
    -orient vertical \
    -command {.console_frame.text yview}

# The debug frame
ttk::labelframe .debug_frame -text "Debug" \
    -labelanchor n \
    -borderwidth 1 \
    -relief groove

text .debug_frame.text -yscrollcommand {.debug_frame.scroll set} \
    -width 100 \
    -height 10 \
    -font log_font

scrollbar .debug_frame.scroll -orient vertical -command {.debug_frame.text yview}

######################## Position everything #########################

# Column 0 will change size as the window is resized
grid columnconfigure . 0 -weight 1

set root_window_row 0
set led_row_limit 5

# Status frame
incr root_window_row
grid config .status_frame \
    -column 0 \
    -row $root_window_row \
    -columnspan 1 -rowspan 1 \
    -padx $widget_padding -pady $widget_padding \
    -sticky "snew"

# Path settings frame
set status_frame_column 0
grid config .status_frame.path_settings_frame \
    -column 0 \
    -row 0 \
    -columnspan 1 -rowspan 1 \
    -padx $widget_padding -pady $widget_padding \
    -sticky "ns"

grid columnconfigure .status_frame $status_frame_column -weight 1

set frame_row 0
set frame_column 0
foreach {led label} $path_settings_items {
    grid config $led \
	-column $frame_column \
	-row $frame_row \
	-padx $widget_padding -pady $widget_padding

    grid config $label \
	-column [expr $frame_column + 1] \
	-row $frame_row \
	-sticky "w" \
	-padx $widget_padding -pady $widget_padding
    incr frame_row
    if {$frame_row == $led_row_limit} {
	incr frame_column 2
	set frame_row 0
    }
}

# Hardware status frame
incr status_frame_column
grid config .status_frame.hardware_status_frame \
    -column 1 \
    -row 0 \
    -columnspan 1 -rowspan 1 \
    -padx $widget_padding -pady $widget_padding \
    -sticky "ns"

grid columnconfigure .status_frame $status_frame_column -weight 1

set frame_row 0
set frame_column 0
foreach {led label} $hardware_status_items {

    grid config $led \
	-column $frame_column \
	-row $frame_row \
	-padx $widget_padding -pady $widget_padding

    grid config $label \
	-column [expr $frame_column + 1] \
	-row $frame_row \
	-sticky "w" \
	-padx $widget_padding -pady $widget_padding

    incr frame_row
    if {$frame_row == $led_row_limit} {
	incr frame_column 2
	set frame_row 0
    }
}

# Provisioning tasks frame
incr status_frame_column
grid config .status_frame.provisioning_tasks_frame \
    -column 2 \
    -row 0 \
    -columnspan 1 -rowspan 1 \
    -padx $widget_padding -pady $widget_padding \
    -sticky "ns"

grid columnconfigure .status_frame $status_frame_column -weight 1

set frame_row 0
set frame_column 0
foreach {led label} $provisioning_task_items {
    incr frame_row
    grid config $led \
	-column $frame_column \
	-row $frame_row \
	-padx $widget_padding -pady $widget_padding

    grid config $label \
	-column [expr $frame_column + 1] \
	-row $frame_row \
	-sticky "w" \
	-padx $widget_padding -pady $widget_padding

    incr frame_row
    if {$frame_row == $led_row_limit} {
	incr frame_column 2
	set frame_row 0
    }
}

# Console frame
incr root_window_row
grid config .console_frame \
    -column 0 \
    -row $root_window_row \
    -columnspan 1 -rowspan 1 \
    -padx $widget_padding -pady $widget_padding \
    -sticky "snew"

grid config .console_frame.text \
    -column 0 \
    -row 0 \
    -columnspan 1 -rowspan 1 \
    -padx $widget_padding -pady $widget_padding \
    -sticky "snew"

grid config .console_frame.scrollbar \
    -column 0 \
    -row 0 \
    -columnspan 1 -rowspan 1 \
    -padx $widget_padding -pady $widget_padding \
    -sticky "nse"

# Allow the console frame and text widget to expand
grid rowconfigure . $root_window_row -weight 1
grid rowconfigure .console_frame 0 -weight 1
grid columnconfigure .console_frame 0 -weight 1

# Debug frame
incr root_window_row
grid config .debug_frame \
    -column 0 \
    -row $root_window_row \
    -columnspan 1 -rowspan 1 \
    -padx $widget_padding -pady $widget_padding \
    -sticky "sew"

pack .debug_frame.scroll -fill y -side right
pack .debug_frame.text -fill x -side bottom -fill both -expand true

# Exit when the window is closed
bind . <Destroy> exit
