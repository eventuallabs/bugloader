toplevel .programmer_tool
menu .programmer_tool.menubar
.programmer_tool configure -menu .programmer_tool.menubar -height 150

if {[dict get $state_dict isp_programmer] ne ""} {
    # A programmer has been specified
    set programmer_name [dict get $state_dict isp_programmer]
} else {
    set programmer_name "(none)"
}
wm title .programmer_tool "Device programming with $programmer_name"


# Help menu
menu .programmer_tool.menubar.help -tearoff 0
.programmer_tool.menubar add cascade -label Help -menu .programmer_tool.menubar.help -underline 0
.programmer_tool.menubar.help add command -label "About the path editor..." \
    -underline 0 -command programmer_tool::help

namespace eval programmer_tool {

    variable entry_width 80

    variable fuse_entry_width 2

    variable boot_hex_path [file tail [dict get $state_dict boot_hex_path]]

    variable high_fuse [string toupper [string trim [dict get $state_dict high_fuse_hex] "0xX"]]

    variable low_fuse [string toupper [string trim [dict get $state_dict low_fuse_hex] "0xX"]]

    variable extended_fuse [string toupper [string trim [dict get $state_dict extended_fuse_hex] "0xX"]]

    variable app_hex_path [file tail [dict get $state_dict app_hex_path]]

    proc cancel {} {
	# Kill the window without writing the settings
	destroy .programmer_tool
    }
}


########################### Create widgets ###########################

# fuse widget list will be a list with [label label entry] widgets
set fuse_widget_list [list]

# Fuse frame
ttk::labelframe .programmer_tool.fuse_frame -text "Fuses" \
    -labelanchor n \
    -borderwidth 1 \
    -relief groove

# High fuse
lappend fuse_widget_list [ttk::label .programmer_tool.fuse_frame.high_fuse_label \
			 -text "High fuse:"]

lappend fuse_widget_list [ttk::label .programmer_tool.fuse_frame.high_fuse_0x_label \
			 -text "0x" \
			 -font big_fixed_font]

lappend fuse_widget_list [ttk::entry .programmer_tool.fuse_frame.high_fuse_entry \
			 -textvariable programmer_tool::high_fuse \
			 -width $programmer_tool::fuse_entry_width \
			 -justify right \
			 -font big_fixed_font \
			 -state readonly]

# Low fuse
lappend fuse_widget_list [ttk::label .programmer_tool.fuse_frame.low_fuse_label \
			 -text "Low fuse:"]

lappend fuse_widget_list [ttk::label .programmer_tool.fuse_frame.low_fuse_0x_label \
			 -text "0x" \
			 -font big_fixed_font]

lappend fuse_widget_list [ttk::entry .programmer_tool.fuse_frame.low_fuse_entry \
			 -textvariable programmer_tool::low_fuse \
			 -width $programmer_tool::fuse_entry_width \
			 -justify right \
			 -font big_fixed_font \
			 -state readonly]


# Extended fuse
lappend fuse_widget_list [ttk::label .programmer_tool.fuse_frame.extended_fuse_label \
			 -text "Extended fuse:"]

lappend fuse_widget_list [ttk::label .programmer_tool.fuse_frame.extended_fuse_0x_label \
			 -text "0x" \
			 -font big_fixed_font]

lappend fuse_widget_list [ttk::entry .programmer_tool.fuse_frame.extended_fuse_entry \
			 -textvariable programmer_tool::extended_fuse \
			 -width $programmer_tool::fuse_entry_width \
			 -justify right \
			 -font big_fixed_font \
			 -state readonly]


# Fuse burn button
ttk::button .programmer_tool.fuse_frame.burn_button \
    -text "Burn" \
    -command programmer::burn_fuses

# Flash memory frame
ttk::labelframe .programmer_tool.flash_frame -text "Flash memory" \
    -labelanchor n \
    -borderwidth 1 \
    -relief groove


# flash widget list will be a list with [label entry button] widgets

# Application
lappend flash_widget_list [ttk::label .programmer_tool.flash_frame.application_label \
			       -text "Application"]

lappend flash_widget_list [ttk::entry .programmer_tool.flash_frame.application_path_entry \
			       -textvariable programmer_tool::app_hex_path \
			       -width $programmer_tool::entry_width \
			       -justify left \
			       -state readonly]

lappend flash_widget_list [ttk::button .programmer_tool.flash_frame.app_hex_flash_button \
			       -text "Flash" \
			       -command programmer::flash_application]

# Test button
ttk::button .programmer_tool.programming_test_button \
    -text "Test" \
    -command programmer::test_programming

# Cancel button
ttk::button .programmer_tool.cancel_button \
    -text "Cancel" \
    -command programmer_tool::cancel

########################## Position widgets ##########################

# Column 0 will change size as the window is resized
grid columnconfigure .programmer_tool 0 -weight 1



set programmer_tool_row 0

grid config .programmer_tool.fuse_frame \
    -column 0 \
    -row $programmer_tool_row \
    -columnspan 2 -rowspan 1 \
    -padx $widget_padding -pady $widget_padding \
    -sticky "ew"

grid columnconfigure .programmer_tool.fuse_frame 2 -weight 1

set fuse_row 0

foreach {name label entry} $fuse_widget_list {
    grid config $name \
	-column 0 -row $fuse_row \
	-sticky "e" \
	-padx 5 -pady 5

    grid config $label \
	-column 1 -row $fuse_row \
	-sticky "w" \
	-padx 0 -pady 5

    grid config $entry \
	-column 2 -row $fuse_row \
	-sticky "w" \
	-padx $widget_padding -pady $widget_padding

    incr fuse_row
    
}

# Burn button
grid config .programmer_tool.fuse_frame.burn_button \
    -column 3 -row 1 \
    -padx $widget_padding -pady $widget_padding

incr programmer_tool_row

grid config .programmer_tool.flash_frame \
    -column 0 \
    -row $programmer_tool_row \
    -columnspan 2 -rowspan 1 \
    -padx $widget_padding -pady $widget_padding \
    -sticky "ew"

grid columnconfigure .programmer_tool.flash_frame 1 -weight 1

set flash_row 0

foreach {label entry button} $flash_widget_list {
    grid config $label \
	-column 0 -row $flash_row \
	-padx $widget_padding -pady $widget_padding

    grid config $entry \
	-column 1 -row $flash_row \
	-sticky "ew" \
	-padx $widget_padding -pady $widget_padding

    grid config $button \
	-column 2 -row $flash_row \
	-padx $widget_padding -pady $widget_padding

    incr flash_row
}


# Cancel button
incr programmer_tool_row
grid config .programmer_tool.cancel_button \
    -column 1 -row $programmer_tool_row \
    -sticky "e" \
    -padx 5 -pady 5

# Test programming button
grid config .programmer_tool.programming_test_button \
    -column 0 -row $programmer_tool_row \
    -sticky "e" \
    -padx 5 -pady 5




# Get rid of the namespace when the window is closed
bind .programmer_tool <Destroy> {
    # tcl calls this script multiple times.  Only delete the namespace
    # if it still exists.
    if {[namespace exists programmer_tool]} {
	namespace delete programmer_tool
    }
}
