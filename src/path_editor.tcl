toplevel .path_editor
menu .path_editor.menubar
.path_editor configure -menu .path_editor.menubar -height 150

wm title .path_editor "Set executable paths"

# Help menu
menu .path_editor.menubar.help -tearoff 0
.path_editor.menubar add cascade -label Help -menu .path_editor.menubar.help -underline 0
.path_editor.menubar.help add command -label "About the path editor..." \
    -underline 0 -command path_editor::help

namespace eval path_editor {

    # Width of the path entry box
    variable entry_width 80

    variable avrdude_path [dict get $state_dict avrdude_path]

    variable app_hex_path [dict get $state_dict app_hex_path]

    proc help {} {
	# What to execute when Help-->About is selected
	#
	# Arguments:
	#   None
	global log
	tk_messageBox -message "Path editor" \
	    -title "Samloader path editor"
    }

    proc set_avrdude_path {} {
	# This will just set the value in the entry field.  Entries
	# won't be active until the OK button is pressed.
	global log
	global state_dict
	if {$::tcl_platform(os) eq "Windows NT"} {
	    set avrdude_path [tk_getOpenFile -defaultextension ".exe" -title "Select avrdude.exe"]    
	} else {
	    set avrdude_path [tk_getOpenFile -title "Select avrdude executable"]
	}
	
	# Erase the old value
	.path_editor.avrdude_path_entry delete 0 end
	# Write the new value
	.path_editor.avrdude_path_entry insert 0 $avrdude_path
	raise .path_editor
    }

    proc set_app_hex_path {} {
	# This will just set the value in the entry field.  Entries
	# won't be active until the OK button is pressed.
	global log
	global state_dict
	set app_hex_path [tk_getOpenFile -defaultextension ".hex" -title "Select application hex file"]
	# Erase the old value
	.path_editor.app_hex_path_entry delete 0 end
	# Write the new value
	.path_editor.app_hex_path_entry insert 0 $app_hex_path
	raise .path_editor
    }
    

    proc write_db_values {} {
	# This will write the values to the database and to the global
	# state dictionary.  It will also kill the window.
	global log
	global state_dict
	# Open a connection to the database file
	sqlite3 db $settings::database_file

	# The avrdude path
	db eval "UPDATE paths SET path='$path_editor::avrdude_path' WHERE program='avrdude'"
	dict set state_dict avrdude_path $path_editor::avrdude_path

	# The application path
	db eval "UPDATE paths SET path='$path_editor::app_hex_path' WHERE program='application'"
	dict set state_dict app_hex_path $path_editor::app_hex_path
	

	# Update the root window LEDs
	settings::check_avrdude_path
	settings::check_app_hex_path

	# Close the window
	destroy .path_editor

    }

    proc cancel {} {
	# Kill the window without writing the settings
	destroy .path_editor
    }

}

########################### Create widgets ###########################

# Path widget list will be a list with [label entry button] widgets
set path_widget_list [list]

# avrdude path label
lappend path_widget_list [ttk::label .path_editor.avrdude_path_label -text "AVRDUDE"]

# avrdude path entry
lappend path_widget_list [ttk::entry .path_editor.avrdude_path_entry \
			      -textvariable path_editor::avrdude_path \
			      -width $path_editor::entry_width \
			      -justify left]

# avrdude path button
lappend path_widget_list [ttk::button .path_editor.avrdude_path_button \
			      -text "..." \
			      -command path_editor::set_avrdude_path]



# Application hex path label
lappend path_widget_list [ttk::label .path_editor.app_hex_path_label -text "Application hex"]

# Application hex path entry
lappend path_widget_list [ttk::entry .path_editor.app_hex_path_entry \
			      -textvariable path_editor::app_hex_path \
			      -width $path_editor::entry_width \
			      -justify left]

# Application hex path button
lappend path_widget_list [ttk::button .path_editor.app_hex_path_button \
			      -text "..." \
			      -command path_editor::set_app_hex_path]

# OK / cancel buttons
ttk::button .path_editor.ok_button \
    -text "OK" \
    -command path_editor::write_db_values

ttk::button .path_editor.cancel_button \
    -text "Cancel" \
    -command path_editor::cancel

########################## Position widgets ##########################

# Allow the path entry to resize
grid columnconfigure .path_editor 1 -weight 1

set path_editor_row 0
foreach {label entry button} $path_widget_list {
    grid config $label \
	-column 0 -row $path_editor_row \
	-padx $widget_padding -pady $widget_padding

    grid config $entry \
	-column 1 -row $path_editor_row \
	-sticky "ew" \
	-padx $widget_padding -pady $widget_padding

    grid config $button \
	-column 2 -row $path_editor_row \
	-padx $widget_padding -pady $widget_padding

    incr path_editor_row
}

# OK / cancel buttons
incr path_editor_row
grid config .path_editor.ok_button \
    -column 1 -row $path_editor_row \
    -sticky "e" \
    -padx 5 -pady 5

grid config .path_editor.cancel_button \
    -column 2 -row $path_editor_row \
    -padx 5 -pady 5
