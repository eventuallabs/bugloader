# Procedures and variables related to the hardware (non-bootloader) programmers.
#
# Bugloader supports two programmers: the Atmel-ICE and the Pololu USB
# AVR programmer v2.1.  The list is limited because the ATmega2560 has
# more flash than most programmers can handle.
namespace eval programmer {

    variable fuse_directory "fuses"
    variable high_fuse_file "high_fuse.hex"
    variable low_fuse_file "low_fuse.hex"
    variable extended_fuse_file "extended_fuse.hex"

    variable high_fuse [dict get $state_dict high_fuse_hex]
    variable low_fuse [dict get $state_dict low_fuse_hex]
    variable extended_fuse [dict get $state_dict extended_fuse_hex]
    
    proc test_programming {} {
	global state_dict
	global log
	test_usb_connection
	test_isp_connection
	test_fuses
	
    }

    proc get_pololu_vcp {} {
	# Return the and set the state variable to the VCP
	# corresponding to the Pololu programmer.
	#
	# The Pololu programmer communicates using a Virtual COM Port
	# (VCP).  Avrdude knows how to use it.
	global log
	global state_dict
	set usb_list [usb::detect_ports]
	# usb::detect_ports will return lists like:
	#
	# COM20 usb-8&15cdfc2e&0&0001
	#
	# for the Pololu USB AVR programmer v2.1.
	foreach {identifier} $usb_list {
	    ${log}::debug "USB device: $identifier"
	    set prefix [lindex [split [lindex [split $identifier " "] 1] "-"] 0]
	    ${log}::debug "Prefix: $prefix"
	    set vcp [lindex [split $identifier " "] 0]
	    ${log}::debug "VCP: $vcp"
	    set serial [lindex [split $identifier "-"] end]
	    if {$prefix eq "usb"} {
		# The Pololu device has a usb prefix, while FTDI
		# devices will have a ftdi prefix.
		if {[string index $serial end] == 1} {
		    # Pololu programmers show up as composite devices
		    # -- they emit two separate serial numbers.  The
		    # device with a serial number ending in 1 seems to
		    # be the one that does programming.
		    ${log}::debug "Found Pololu v2.1 at $vcp"
		    dict set state_dict isp_programmer_vcp $vcp
		    return $vcp
		}
	    }
	}
	dict set state_dict isp_programmer_vcp ""
	return ""
    }

    proc test_usb_connection {} {
	# Try to talk to the programmer
	global state_dict
	global log
	global isp_programmer_status_led_canvas
	${log}::debug "Testing connection to [dict get $state_dict isp_programmer]"
	if {[dict get $state_dict avrdude_path] == ""} {
	    # There's no avrdude exectuable
	    set found_programmer false
	} else {
	    # Try to talk to the programmer
	    if {[dict get $state_dict isp_programmer] eq "Atmel ICE"} {
		set port_param usb
		set c_param atmelice_isp
	    } elseif {[dict get $state_dict isp_programmer] eq "Pololu v2.1"} {
		set port_param [get_pololu_vcp]
		set c_param avrispv2
	    }
	    catch "exec [dict get $state_dict avrdude_path] -P $port_param -c $c_param -p m2560" result
	    text_console::manager $result
	    # avrdude will emit this string if it fails to find the Atmel-ICE programmer
	    set fail_string "Did not find any device matching VID 0x03eb and PID list: 0x2141"
	    set found_programmer true
	    # Look for the programmer.
	    foreach line [split $result "\n"] {
		if {[string first $fail_string $line] >= 0} {
		    # The programmer wasn't found
		    set found_programmer false
		}
	    }  
	}

	if $found_programmer {
	    status_leds::set_led $isp_programmer_status_led_canvas true
	    ${log}::info "Found Atmel-ICE programmer"
	    dict set state_dict programer_connected_via_usb true
	} else {
	    status_leds::set_led $isp_programmer_status_led_canvas false
	    ${log}::error "Did not find Atmel-ICE programmer"
	    dict set state_dict programer_connected_via_usb false
	}
    }



    proc test_isp_connection {} {
	# Try to read a device signature.  If that succeeds, we're connected via ISP
	global state_dict
	global log
	global isp_connection_status_led_canvas
	global isp_programmer_status_led_canvas
	if {[dict get $state_dict isp_programmer] eq "Atmel ICE"} {
	    set port_param usb
	    set c_param atmelice_isp
	} elseif {[dict get $state_dict isp_programmer] eq "Pololu v2.1"} {
	    set port_param [get_pololu_vcp]
	    set c_param avrispv2
	}
	set avrdude_exec_string "exec [dict get $state_dict avrdude_path] "
	append avrdude_exec_string "-P $port_param -c $c_param -p [dict get $state_dict avrdude_mcu_name] "
	catch $avrdude_exec_string result
	set success_string "Device signature = 0x1e950f (probably m328p)"
	set isp_ok false
	foreach line [split $result "\n"] {
	    if {[string first $success_string $line] >= 0} {
		# We have an ISP connection
		set isp_ok true
	    }
	}
	if $isp_ok {
	    # If we could read the device signature, the ISP
	    # connection is OK and the ISP programmer connection is
	    # OK.
	    status_leds::set_led $isp_connection_status_led_canvas true
	    status_leds::set_led $isp_programmer_status_led_canvas true
	    ${log}::info "ISP connection to ATmega2560 OK"
	    dict set state_dict programmer_connected_via_isp true
	    dict set state_dict programer_connected_via_usb true
	} else {
	    status_leds::set_led $isp_connection_status_led_canvas false
	    ${log}::error "Failed to make ISP connection to ATmega2560"
	    dict set state_dict programmer_connected_via_isp false
	}
    }

    proc get_fuse_dict {} {
	# Read the fuses from the part and return a dictionary of fuse values.
	global state_dict
	global log
	if {![dict get $state_dict programmer_connected_via_isp]} {
	    # We're not connected
	    ${log}::error "Programmer not connected via ISP -- can't read fuses"
	    dict set fuse_dict high_fuse 0
	    dict set fuse_dict low_fuse 0
	    dict set fuse_dict extended_fuse 0
	    return $fuse_dict
	}
	file mkdir $programmer::fuse_directory
	set avrdude_exec_string "exec [dict get $state_dict avrdude_path] "
	if {[dict get $state_dict isp_programmer] eq "Atmel ICE"} {
	    set port_param usb
	    set c_param atmelice_isp
	} elseif {[dict get $state_dict isp_programmer] eq "Pololu v2.1"} {
	    set port_param [get_pololu_vcp]
	    set c_param avrispv2
	}
	append avrdude_exec_string "-P $port_param -c $c_param -p [dict get $state_dict avrdude_mcu_name] "
	# Read high fuse to file
	append avrdude_exec_string "-U hfuse:r:${programmer::fuse_directory}/${programmer::high_fuse_file}:h "
	# Read low fuse to file
	append avrdude_exec_string "-U lfuse:r:${programmer::fuse_directory}/${programmer::low_fuse_file}:h "
	# Read extended fuse to file
	append avrdude_exec_string "-U efuse:r:${programmer::fuse_directory}/${programmer::extended_fuse_file}:h "
	catch $avrdude_exec_string result
	text_console::manager $result

	# Read the values
	set fid [open ${programmer::fuse_directory}/${programmer::high_fuse_file} r]
	dict set fuse_dict high_fuse [string trim [read $fid]]
	${log}::debug "High fuse is [dict get $fuse_dict high_fuse]"
	close $fid

	set fid [open ${programmer::fuse_directory}/${programmer::low_fuse_file} r]
	dict set fuse_dict low_fuse [string trim [read $fid]]
	${log}::debug "Low fuse is [dict get $fuse_dict low_fuse]"
	close $fid

	set fid [open ${programmer::fuse_directory}/${programmer::extended_fuse_file} r]
	dict set fuse_dict extended_fuse [string trim [read $fid]]
	${log}::debug "Extended fuse is [dict get $fuse_dict extended_fuse]"
	close $fid

	# Clean up
	file delete -force ${programmer::fuse_directory}

	return $fuse_dict
    }

    proc test_fuses {} {
	global log
	global state_dict
	global fuse_task_led_canvas
	set fuse_dict [get_fuse_dict]
	if {[dict get $fuse_dict high_fuse] == $programmer::high_fuse} {
	    ${log}::debug "High fuse programmed"
	    set high_fuse_programmed true
	} else {
	    set high_fuse_programmed false
	}

	if {[dict get $fuse_dict low_fuse] == $programmer::low_fuse} {
	    ${log}::debug "Low fuse programmed"
	    set low_fuse_programmed true
	} else {
	    set low_fuse_programmed false
	}

	if {[dict get $fuse_dict extended_fuse] == $programmer::extended_fuse} {
	    ${log}::debug "Extended fuse programmed"
	    set extended_fuse_programmed true
	} else {
	    set extended_fuse_programmed false
	}
	if {$high_fuse_programmed && $low_fuse_programmed && $extended_fuse_programmed} {
	    status_leds::set_led $fuse_task_led_canvas true
	    dict set state_dict fuses_set true
	} else {
	    dict set state_dict fuses_set false
	    status_leds::set_led $fuse_task_led_canvas false
	}
	
    }

    proc burn_fuses {} {
	# Write fuses
	global log
	global state_dict
	if {[dict get $state_dict isp_programmer] eq "Atmel ICE"} {
	    set port_param usb
	    set c_param atmelice_isp
	} elseif {[dict get $state_dict isp_programmer] eq "Pololu v2.1"} {
	    set port_param [get_pololu_vcp]
	    set c_param avrispv2
	}
	if {![dict get $state_dict programmer_connected_via_isp]} {
	    # We're not connected.  Try connecting
	    test_isp_connection
	    if {![dict get $state_dict programmer_connected_via_isp]} {
		# We failed to connect after trying
		${log}::error "Programmer not connected via ISP -- can't burn fuses"
		return
	    }	    
	}
	set avrdude_exec_string "exec [dict get $state_dict avrdude_path] "
	append avrdude_exec_string "-P $port_param -c $c_param -p [dict get $state_dict avrdude_mcu_name] "
	# Write high fuse
	append avrdude_exec_string "-U hfuse:w:${programmer::high_fuse}:m "
	# Write low fuse
	append avrdude_exec_string "-U lfuse:w:${programmer::low_fuse}:m "
	# Write extended fuse
	append avrdude_exec_string "-U efuse:w:${programmer::extended_fuse}:m"
	catch $avrdude_exec_string result
	text_console::manager $result
	programmer::test_fuses
	
    }

    proc burn_boot_hex {} {
	# Write fuses
	global log
	global state_dict
	global bootloader_task_led_canvas
	if {![dict get $state_dict programmer_connected_via_isp]} {
	    # We're not connected
	    ${log}::error "Programmer not connected via ISP -- can't burn fuses"
	    return
	}
	set avrdude_exec_string "[dict get $state_dict avrdude_path] "
	if {[dict get $state_dict isp_programmer] eq "Atmel ICE"} {
	    set port_param usb
	    set c_param atmelice_isp
	} elseif {[dict get $state_dict isp_programmer] eq "Pololu v2.1"} {
	    set port_param [get_pololu_vcp]
	    set c_param avrispv2
	}
	append avrdude_exec_string "-P $port_param -c $c_param -p m2560 "
	append avrdude_exec_string "-U flash:w:[dict get $state_dict boot_hex_path]:a"
	

	# catch $avrdude_exec_string result
	set fid [open "| $avrdude_exec_string 2>@1" r]
	while {[gets $fid line] != -1} {
	    # gets returns -1 if it encounters the end of output from
	    # the program.  If it returns > -1, then we've read a line
	    # of output and the characters are stored in the variable
	    # "line".

	    # Process the line read as desired...
	    text_console::manager $line
	    update idletasks
	}
	# Close our side of the pipe when we're done
	close $fid

	# Just assume things went OK for now
	status_leds::set_led $bootloader_task_led_canvas true
	${log}::info "Bootloader has been flashed"
	dict set state_dict bootloader_flashed true
	
    }

    proc flash_application {} {
	global log
	global state_dict
	global application_task_led_canvas
	if {[dict get $state_dict isp_programmer] eq "Atmel ICE"} {
	    set port_param usb
	    set c_param atmelice_isp
	} elseif {[dict get $state_dict isp_programmer] eq "Pololu v2.1"} {
	    set port_param [get_pololu_vcp]
	    set c_param avrispv2
	}
	if {![dict get $state_dict programmer_connected_via_isp]} {
	    # We're not connected.  Try connecting
	    test_isp_connection
	    if {![dict get $state_dict programmer_connected_via_isp]} {
		# We failed to connect after trying
		${log}::error "Programmer not connected via ISP -- can't flash application"
		return
	    }	    
	}
	set avrdude_exec_string "[dict get $state_dict avrdude_path] "
	append avrdude_exec_string "-P $port_param -c $c_param -p [dict get $state_dict avrdude_mcu_name] "
	append avrdude_exec_string "-U \"flash:w:[dict get $state_dict app_hex_path]:a\""
	puts $avrdude_exec_string
	set fid [open "| $avrdude_exec_string 2>@1" r]
	while {[gets $fid line] != -1} {
	    # gets returns -1 if it encounters the end of output from
	    # the program.  If it returns > -1, then we've read a line
	    # of output and the characters are stored in the variable
	    # "line".

	    # Process the line read as desired...
	    text_console::manager $line
	    update idletasks
	}
	# Close our side of the pipe when we're done
	close $fid

	# Just asseme things went OK for now
	status_leds::set_led $application_task_led_canvas true
	${log}::info "Application has been flashed"
	dict set state_dict application_flashed true
	
    }
}
